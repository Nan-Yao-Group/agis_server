#PBS -N bwaMapping
#PBS -l nodes=1:ppn=2
#PBS -l walltime=480:00:00
#PBS -q low
#PBS -e err.log

echo "Start at:"
date
hostname

cd ${PBS_O_WORKDIR}

# bwa mem mapping
genome_home=/public/home/zhusj/reference/G.hirsutum/zju_v2.1
fa=$genome_home/TM-1_V2.1.fa
bwa_idx=$genome_home/index/bwa/TM-1_V2.1
fq_home=/public/home/zhusj/xu142/wgs/cleanreads

bwa mem -t 50 \
	-R "@RG\tID:$sample\tSM:$sample\tLB:WGS\tPL:Illumina" \
	$bwa_idx $fq_home/xu142_1_val_1.fq.gz \
	$fq_home/xu142_2_val_2.fq.gz \
	> ../fq2bam2vcf/xu142.bwa.sam 2>xu142.bwa.log

echo "End at:"
date